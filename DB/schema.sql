#Database for the BAS
#CSCI-311
#Nicholas Boschman
#March 3, 2018

DROP TABLE Spot;
DROP TABLE Users;
DROP TABLE BunPictures;
DROP TABLE Buns;

CREATE TABLE Buns(
   bid int NOT NULL AUTO_INCREMENT,
   tag VARCHAR(50),
   colour VARCHAR(50),
   gender VARCHAR(50),
   PRIMARY KEY (bid)
);

#decided to keep picture in there own table on recomendatio from stackOverflow
#https://stackoverflow.com/questions/5613898/storing-images-in-sql-server
#
# To add pictures we are going to something like the following in PHP
# $sql = "INSERT INTO ImageStore(ImageId,Image)
#         VALUES('$this->image_id','" . mysql_escape_string(file_get_contents($tmp_image)) . "')";
CREATE TABLE BunPictures(
   pid int,
   bid int,
   pic BLOB,
   PRIMARY KEY (pid),
   FOREIGN KEY (bid) REFERENCES Buns (bid)
);

CREATE TABLE Users(
   uid int NOT NULL AUTO_INCREMENT,
   username VARCHAR(50) NOT NULL,
   first_name VARCHAR(50) NOT NULL,
   last_name VARCHAR(50) NOT NULL,
   email VARCHAR(50) NOT NULL,
   password VARCHAR(60) NOT NUll,
   scheme VARCHAR(30) DEFAULT 'daytime',
   PRIMARY KEY (uid),
   UNIQUE (username),
   UNIQUE (email)
);

CREATE TABLE Spot(
   bid int NOT NULL,
   uid int NOT NULL,
   lat FLOAT( 10, 6 ) NOT NULL,
   lng FLOAT( 10, 6 ) NOT NULL,
   spottime DATETIME,
   description VARCHAR(144),
   FOREIGN KEY (bid) REFERENCES Buns(bid),
   FOREIGN KEY (uid) REFERENCES Users(uid)
);
