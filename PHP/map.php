<?php require_once("header.php"); ?>

<!-- https://bootstrapious.com/p/bootstrap-sidebar -->
<div class="wrapper">
    <?php require_once("nav.php"); ?>

    <div id="content">
        <nav id="mobile_navbar" class="navbar navbar-default">
            <div>
                <button type="button" id="sidebarCollapse" class="btn btn-outline-info"> <i class="fas fa-bars"></i></button> 
                <div id="mobile_title"><strong>Bun Alert System</strong></div>
            </div>
        </nav>
        
        <!-- Page content goes here.... -->
        <h2>MAP</h2>
        <div id="map" style="height: 70vh; width: calc(90vw -80px);"></div>
        <br><br>
        <h2>Found Buns</h2>
        <p>Here is where all the Buns have been located!</p>
    <?php require_once("copyright.php"); ?>
    </div>
</div>
<script>
    <?php
        $DBunny = new DBun();
        echo "var buns =".$DBunny->getBunsMap(0,0);
        $DBunny->killConn();
    ?>
</script>
<script>
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar-nav').toggleClass('active');
            $('#content').toggleClass('active');
            $(this).toggleClass('active');
        });
    });
</script>
<!-- jQuery CDN -->
<!-- Bootstrap Js CDN -->


<?php require_once("footer.php"); ?>
