<?php
require_once("header.php"); 
require_once("feedHelpers.php");

if ($status == 0) {
    header("location:login.php");
    exit;
}

$check = false;
if(isset($_POST['submit'])) {
  $rabbit_lat = htmlspecialchars($_POST['rabbit-lat']);
  $rabbit_lng = htmlspecialchars($_POST['rabbit-long']);
  $desc = htmlspecialchars($_POST['locDesc']);
  $color = htmlspecialchars($_POST['bunColor']);
  $gender = htmlspecialchars($_POST['bunGender']);
  $check = postBun($rabbit_lat, $rabbit_lng, $desc, $color, $gender);
}
?>

<!-- https://bootstrapious.com/p/bootstrap-sidebar -->
<div class="wrapper">
    <?php require_once("nav.php"); ?>

    <div id="content">
      <?php   if($check) {
                echo '<script>alert("Your bun has been submitted!");</script>';
              } ?>
        <nav id="mobile_navbar" class="navbar navbar-default">
            <div>
                <button type="button" id="sidebarCollapse" class="btn btn-outline-info"> <i class="fas fa-bars"></i></button> 
                <div id="mobile_title"><strong>Bun Alert System</strong></div>
            </div>
        </nav>
        <!-- Page content goes here.... -->
        <h2>Post A Bun</h2>
        <form class="form-horizontal" method="post" id="bunForm" action="">
        <div class="row">
          <div class="col-md-6">

            <div id="map" class="submit_map"></div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="rabbit-lat">Latitude:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="rabbit-lat" id="rabbit-lat" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="rabbit-long">Longitude:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="rabbit-long" id="rabbit-long" readonly>
              </div>
            </div>
        
          </div>
          <div class="col-md-6">

            <div class="form-group">
              <label class="control-label col-sm-2" for="locDesc">Location Description</label>
              <div class="col-sm-10">
                <textarea class="form-control" id="locDesc" name="locDesc" placeholder="Enter a description of the area." maxlength="144"></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="bunColor">Bun Color</label>
              <div class="col-sm-10">
                <textarea class="form-control" id="bunColor" name="bunColor" placeholder="What color was the fuzzy wuzzy bunny?" maxlength="50"></textarea>
              </div>
            </div>

            <div class="form-group">
              <label for="bunGender" class="control-label col-sm-2">Bun Gender</label>
              <div class="col-sm-10">
                <label for="bunGender">Male</label>
                <input type="radio" name="bunGender" value="male" checked="checked">
              </div>
              <div class="col-sm-10">
                  <label for="bunGender">Female</label>
                  <input type="radio" name="bunGender" value="female">
              </div>
            </div>
            <div class="form-group">        
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default" name="submit">Submit</button>
              </div>
            </div>
            
          </div>
        </div>
        </form>
        
        
    <?php require_once("copyright.php"); ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar-nav').toggleClass('active');
            $('#content').toggleClass('active');
            $(this).toggleClass('active');
        });
    });
</script>
<!-- jQuery CDN -->

<!-- Bootstrap Js CDN -->


<?php require_once("footer.php"); ?>