<?php
/**
 * Created by PhpStorm.
 * User: boutinp
 * Date: 05/04/18
 * Time: 2:43 PM
 */

class DBun {
    const SERVER = "wwwstu.csci.viu.ca";
    const DBUSER = "csci311a";
    const PASS = "w37ezgqg";
    const DATABASE = "csci311a";

    private $pdo;

    function __construct() {
        try {
            // Put your database information
            $this->pdo = new PDO("mysql:host=".self::SERVER.";dbname=".self::DATABASE, self::DBUSER, self::PASS);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    private function generate_salt($length) {
        //generate pseudo random string (good enough)
        //returns 32 characters
        $unique_random_string = md5(uniqid(mt_rand(), true));
        //convert it to base 64 (valid chars are [a-zA-Z0-0./] )
        $base64_string = base64_encode($unique_random_string);
        //remove the '+' characters, just replace with '.'
        $good_base64_string = str_replace('+', '.', $base64_string);

        $salt = substr($good_base64_string, 0, $length);
        //truncate off just what we need
        return $salt;
        //return it
    }

    private function password_encrypt($password) {
        $hash_format = "$2y$10$";
        $salt = $this->generate_salt(22);
        $format_and_salt = $hash_format . $salt;
        $hash = crypt($password, $format_and_salt);
        return $hash;
    }

    private function password_check($password, $existing_hash) {
        $hash = crypt($password, $existing_hash);
        if($hash === $existing_hash){
            return true;
        }else{
            return false;
        }
    }

    public function killConn() {
        $this->pdo = null;
    }

    /*
     * Function that gets a certain ammount of buns
     * @param $start -
     * @param $amount -
     * @return - json encoded list of buns
     */
    public function getBunsMap($start = 0, $amount = 0) {
        try{
            $query = "SELECT bid, lat, lng, spottime, description, colour, gender FROM Buns NATURAL JOIN Spot;";

            $stmnt = $this->pdo->prepare($query);

            $stmnt->execute();

            return json_encode($stmnt->fetchAll());
        } catch(PDOException $e) {
            $err = "Connection failed: ".$e->getMessage() . "\n";
            return false;
        }
    }

    public function accountExists($username) {
        try {
            //check that the user exists
            $myStmt = $this->pdo->prepare("SELECT count(*) as total FROM Users WHERE username=:u_name");
            $myStmt->bindParam(':u_name', $username);
            $myStmt->execute();
            $count = $myStmt->fetchColumn();
            if($count == 0){
                return true;
            } else {
                return false;
            }
        } catch(PDOException $e) {
            $err = "Connection failed: ".$e->getMessage() . "\n";
            return false;
        }
    }

    public function createAccount($username, $firstname, $lastname, $pw, $email) {
        try{
            //username does not exist yet, so let's create it
            $sql = "INSERT into Users(username, first_name, last_name, email, password) VALUES(:u_name, :f_name, :l_name, :email, :p_word)";
            $insertStmt = $this->pdo->prepare($sql);
            //hash the password
            $hashed_pw = $this->password_encrypt($pw);
            $insertStmt->bindParam(':u_name', $username);
            $insertStmt->bindParam(':f_name', $firstname);
            $insertStmt->bindParam(':l_name', $lastname);
            $insertStmt->bindParam(':email', $email);
            $insertStmt->bindParam(':p_word', $hashed_pw);

            return $insertStmt->execute();
        } catch(PDOException $e) {
            $err = "Connection failed: ".$e->getMessage() . "\n";
            return false;
        }

    }

    public function attempt_login($userID, $passwordAttempt) {
        //check that the user exists
        //connect to db,
        try{
            //make sure user exists
            $hashed_pw = "";
            $myStmt = $this->pdo->prepare("SELECT username, password FROM Users WHERE username=:u_name");
            $myStmt->bindParam(':u_name', $userID);
            $myStmt->execute();
            $rslt = $myStmt->fetchAll();
            if(count($rslt) > 0){
                //good
                //we have the hashed password for this user
                //check it against the one they entered
                foreach($rslt as $row){
                    $hashed_pw = $row['password'];
                }

                //then check it
                if($this->password_check($passwordAttempt, $hashed_pw)){
                    return true;
                }else{
                    return false;

                }
            }else{
                return false;
            }
        } catch(PDOException $e) {
            $err = "Connection failed: ".$e->getMessage() . "\n";
            return false;
        }

    }

    public function populateFeed() {
        try {
            $query = 'SELECT lat, lng, spottime, description, colour, gender FROM Spot NATURAL JOIN Buns ORDER BY spottime DESC';
            $foundBuns = $this->pdo->prepare($query);
            $foundBuns->execute();
            $result = $foundBuns->fetchAll();

            return $result;
        } catch(PDOException $e) {
            $err = "Connection failed: ".$e->getMessage() . "\n";
            return false;
        }

    }

    public function insertBun($rabbit_lat, $rabbit_lng, $desc, $color, $gender) {
        try {
            if (empty($_SESSION['UserData']['Username'])){
                return false;
            }

            $uid = $this->getUserId($_SESSION['UserData']['Username']);
            $bid = $this->getMaxBunId();

            if (!$bid || !$uid){
                return false;
            }

            $insertIntoBuns = 'INSERT INTO Buns (`tag`, `colour`, `gender`) VALUES (:tag, :colour, :gender)';
            $prep = $this->pdo->prepare($insertIntoBuns);
            $prep->execute([':tag'=>NULL, ':colour'=>$color, ':gender'=>$gender]);

            $insertIntoSpot = 'INSERT INTO Spot (`bid`, `uid`, `lat`, `lng`, `spottime`, `description`) VALUES (:bid, :uid, :lat, :lng, SYSDATE(), :description)';
            $prep2 = $this->pdo->prepare($insertIntoSpot);

            return $prep2->execute([':bid'=>$bid[0]["bid"]+1, ':uid'=>$uid[0]["uid"], ':lat'=>$rabbit_lat, ':lng'=>$rabbit_lng, ':description'=>$desc]);
        } catch(PDOException $e) {
            $err = "Connection Failed @insertBun: ".$e->getMessage()."\n";
            return false;
        }
    }

    public function getMaxBunId () {
        try {
            return $this->pdo->query('SELECT MAX(bid) as \'bid\' FROM Buns')->fetchAll();
        } catch(PDOException $e) {
            $err = "Connection Failed @insertBun: ".$e->getMessage()."\n";
            return false;
        }
    }

    public function getUserId($username) {
        try {
            $uidquery = $this->pdo->prepare('SELECT uid FROM Users WHERE username = :usernm');
            $uidquery->execute([':usernm'=>$username]);

            return $uidquery->fetchAll();
        } catch(PDOException $e) {
            $err = "Connection Failed @insertBun: ".$e->getMessage()."\n";
            return false;
        }
    }

    public function getAccountInfo($username){
        try{
            $myStmt = $this->pdo->prepare("SELECT * FROM Users WHERE username=:u_name");
            $myStmt->bindParam(':u_name', $username);
            $myStmt->execute();
            $rslt = $myStmt->fetchAll();
            return $rslt[0];
        }catch(PDOException $e){
            $err = "Connection failed: " . $e->getMessage() . "\n";
            return false;
        }
    }

    public function changeAccountTheme($username, $theme) {
        try{
            $myStmt = $this->pdo->prepare("UPDATE Users set scheme=:theme WHERE username=:u_name");
            $myStmt->bindParam(':u_name', $username);
            $myStmt->bindParam(':theme', $theme);
            return $myStmt->execute();
        }catch(PDOException $e){
            $err = "Connection failed: " . $e->getMessage() . "\n";
            return false;
        }

    }

    public function getAccountTheme($username) {
        try{
            $myStmt = $this->pdo->prepare("SELECT scheme FROM Users WHERE username=:u_name");
            $myStmt->bindParam(':u_name', $username);
            $myStmt->execute();
            $rslt = $myStmt->fetchAll();
            return $rslt[0]['scheme'];
        }catch(PDOException $e){
            $err = "Connection failed: " . $e->getMessage() . "\n";
            return false;
        }

    }

}