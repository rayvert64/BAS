<?php
session_start();
$theme = 'daytime';
$status;
require_once("class/DBun.php");
if(isset($_SESSION['UserData']['Username'])) {
    $DBunny = new DBun();
    $theme = $DBunny->getAccountTheme($_SESSION['UserData']['Username']);
    $DBunny->killConn();
    $status = 1;
} else {
    $status = 0;
}
$page = explode('.',basename($_SERVER['PHP_SELF'])); 
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <?php 
    if($page[0] == 'index') {
        echo '<link rel="stylesheet" type="text/css" href="./CSS/template_bs4.css">';
        if($theme == 'nighttime'){
            echo '<link rel="stylesheet" type="text/css" href="./CSS/night.css">';
        }
    } elseif ($page[0] == 'map') {
        echo '<link rel="stylesheet" type="text/css" href="../CSS/template_bs4.css">';
        echo '<style>
                #map {
                    width: calc(100%-80px);
                    height: 400px;
                    background-color: grey;
                }
            </style>';
        if($theme == 'nighttime'){
            echo '<link rel="stylesheet type="text/css href="../CSS/night.css">';
        }

    } else {
        echo '<link rel="stylesheet" type="text/css" href="../CSS/template_bs4.css">';
        if($theme == 'nighttime'){
            echo '<link rel="stylesheet type="text/css href="../CSS/night.css">';
        }
    }
    ?>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
    <title><?php echo "$page[0]"; ?> </title>
</head>
<body>

