<?php
    $page = basename($_SERVER['PHP_SELF']);
?>

<nav id="sidebar-nav">
        <!-- SideBar Header -->
        <div class="sidebar-header"> <strong>BAS</strong>
        </div>
        <ul class="list-unstyled components">
            <!-- HOME NAV BAR BUTTON -->
            <div id="scrollable-navigation">
            <li <?php if($page == 'index.php'){
                        echo 'class="active"';
                      }
                ?>
            > 
                <a href=<?php
                    if($page == 'index.php') {
                        echo '"index.php"';
                    } else {
                        echo '"../index.php"';
                    }
                ?>
                >
                    <i class="fas fa-home fa-lg"></i>
                    Home
                </a>
            </li>

            <!-- MAP NAV BAR BUTTON -->
            <li <?php if($page == 'map.php'){
                        echo 'class="active"';
                      }
                ?>
            > 
                <a href= <?php 
                    if($page == 'index.php') {
                        echo '"PHP/map.php"';
                    } else {
                        echo '"map.php"';
                    }
                ?>
                >
                    <i class="fas fa-map-pin fa-2x"></i>
                    MAP
                </a>
            </li> 

             <!-- FEED NAV BAR BUTTON -->
            <li <?php if($page == 'feed.php'){
                        echo 'class="active"';
                      }
                ?>

            > 
                <a href=<?php
                    if($page == 'index.php') {
                        echo '"PHP/feed.php"';
                    } else {
                        echo '"feed.php"';
                    }
                ?>
                >
                    <i class="fas fa-rss-square fa-lg"></i>
                    FEED
                </a>
            </li>

            <!-- ADD BUN NAV BUTTON -->
            <li <?php if($page == 'makePost.php'){
                        echo 'class="active"';
                      }
                ?>
            > 
                <a href=
                <?php
                if($status == 1) {
                    if($page == 'index.php') {
                        echo '"PHP/makePost.php"';
                    } else {
                        echo '"makePost.php"';
                    }
                } else {
                    if($page == 'index.php') {
                        echo '"PHP/login.php"';
                    } else {
                        echo '"login.php"';
                    }
                }
                ?>
                >
                    <i class="fas fa-plus fa-lg"></i>
                    +BUN
                </a>
            </li>

            <!-- EXIT ADDBUN NAV BAR BUTTON -->

            <!-- ABOUT NAV BAR BUTTON -->
            <li <?php if($page == 'about.php'){
                        echo 'class="active"';
                      }
                ?>
            > 
                <a href=<?php 
                    if($page == 'index.php') {
                        echo '"PHP/about.php"';
                    } else {
                        echo '"about.php"';
                    }

                ?>
                >
                    <i class="fas fa-briefcase fa-lg"></i>
                    ABOUT
                </a>
            </li>
            </div>
            <div>
            <?php

            if($status == 1){
            ?>
            <li id="exitNav"> 
                <a href=<?php 
                    if($page == 'index.php') {
                        echo '"PHP/logout.php"';
                    } else {
                        echo '"logout.php"';
                    }

                ?>
                >
                    <i class="fas fa-sign-out-alt fa-lg"></i>
                    EXIT
                </a>
            </li>
            <?php } ?>  

             <!-- SETTINGS NAV BAR BUTTON -->
            <li id="settings"> 
                <a href=
                <?php
                    if($page == 'index.php') {
                        echo '"PHP/account.php"';
                    } else {
                        echo '"account.php"';
                    }
                ?>
                >
                    <i class="fas fa-user-circle fa-lg"></i>
                    <?php
                    if($status == 1){
                        echo "ACC";
                    }else{
                        echo "LOGIN";
                    } 
                     ?>
                </a>
            </li>
            </div>
        </ul>
    </nav>

