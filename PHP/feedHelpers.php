<?php
/**
 * Created by PhpStorm.
 * User: boutinp
 * Date: 02/04/18
 * Time: 9:26 PM
 */

function postBun($rabbit_lat, $rabbit_lng, $desc, $color, $gender) {
    
    $desc = htmlspecialchars($desc);
    $color = htmlspecialchars($color);
    #https://gist.github.com/arubacao/b5683b1dab4e4a47ee18fd55d9efbdd1
    if(preg_match_all('/[\w0-9]+/', $desc) && preg_match_all('/[\w0-9]+/', $color) && preg_match_all('/[\w0-9]+/', $gender) && (preg_match('/^\A(male)\z$/',$gender) || preg_match('/^\A(female)\z$/', $gender))) {
        $bun = new DBun();
        $stat = $bun->insertBun($rabbit_lat, $rabbit_lng, $desc, $color, $gender);
        $bun->killConn();
        return $stat;
    } else {
        echo "Regex Killed it";
        return false;
    }
}