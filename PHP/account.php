<?php
    require_once("header.php");

    if($status == 0){
        header("location:login.php");
        exit;
    }

?>

<!-- https://bootstrapious.com/p/bootstrap-sidebar -->
<div class="wrapper">
    <?php require_once("nav.php"); ?>

    <div id="content">
        <nav id="mobile_navbar" class="navbar navbar-default">
            <div>
                <button type="button" id="sidebarCollapse" class="btn btn-outline-info"> <i class="fas fa-bars"></i></button> 
                <div id="mobile_title"><strong>Bun Alert System</strong></div>
            </div>
        </nav>
        <!-- Page content goes here.... -->
        <h2>Account Page</h2>

        <ul class="list-group accountInfo">
        <?php
            $DBunny = new DBun();
            $rslt = $DBunny->getAccountInfo($_SESSION['UserData']['Username']);
            $DBunny->killConn();
            if(count($rslt) > 0){
                echo '<li class="list-group-item accountInfo"> Username   : ' . htmlspecialchars($rslt['username']) . '</li>';
                echo '<li class="list-group-item accountInfo"> First Name : ' . htmlspecialchars($rslt['first_name']) . '</li>';
                echo '<li class="list-group-item accountInfo"> Last Name  : ' . htmlspecialchars($rslt['last_name']) . '</li>';
                echo '<li class="list-group-item accountInfo"> Email      : ' . htmlspecialchars($rslt['email']) . '</li>';
            }

        ?>
        </ul>
        <br>

        <a href="toggleTheme.php" class="btn btn-info" role="button">Switch Theme</a>
        <br>
        <br>
        <a href="logout.php" class="btn btn-info" role="button">Logout</a>


    <?php require_once("copyright.php"); ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar-nav').toggleClass('active');
            $('#content').toggleClass('active');
            $(this).toggleClass('active');
        });
    });
</script>
<!-- jQuery CDN -->
<!-- Bootstrap Js CDN -->


<?php require_once("footer.php"); ?>