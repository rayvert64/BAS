<?php require_once("header.php"); ?>

<!-- https://bootstrapious.com/p/bootstrap-sidebar -->
<div class="wrapper">
    <?php require_once("nav.php"); ?>

    <div id="content">
        <nav id="mobile_navbar" class="navbar navbar-default">
            <div>
                <button type="button" id="sidebarCollapse" class="btn btn-outline-info"> <i class="fas fa-bars"></i></button> 
                <div id="mobile_title"><strong>Bun Alert System</strong></div>
            </div>
        </nav>
        <!-- Page content goes here.... -->
        <h2>ABOUT</h2>
        <div class="row">
        <div class="col-md-2"><span style="visibility: hidden;">filler</span></div>
        <div class="col-md-10 col-sm-12">
            <img src="../STATIC/IMG/bun_hierarchy.png" class="img-fluid" alt="Bun hierarchy">
        </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2>The History of Bun</h2>
                <p>The Bun is not simply a creature of fuzz and cute... it has a backstory like no other.</p>
                <h3>The Bun Heirarchy</h3>
                <p><h4>The King Bun</h4>
                    <p>As shown in the above figure, the King Bun sits atop the heirachal tree to which the Bun monarchy exists. The King Bun holds all the power and is so illusive that it's existence has been considered a conspiracy.</p> <br>
                    <p>The King Bun is the smallest of the Bun species. It is said that you would be as lucky to see a leprechaun as you would a King Bun since the King Bun is kept so well guarded and hidden from the world as to protect the integral decision making for the underlying Bun System.</p>
                    <br><br>
                    <h4>The Knight Buns</h4>
                    <p>The Knight Buns are larger to the King Bun, but not by much. Do not let their size fool you. They have overwhelming strength and tenacity. A Knight Bun was once contracted to "act" on Monty Python's Holy Grail movie where there was a demonstration of the Knight Bun's feirce brutality. That scene was not used in the movie due to the horror of the Knight Bun but instead a different angle of the same attack. Knight Buns should never be approached.</p>
                    <br><br>
                    <h4>Duke Bun</h4>
                    <p>Duke Buns oversee the structure below them and answer directly to the King Bun in audience with the Knight Buns (The King Bun is always protected 24/7). The Duke Buns are regional landholders who are responsible for their underlying population. This means food, water, and performing wedding ceremonies</p>
                    <br><br>
                    <h4>The Bottom Layer</h4>
                    <h5>Aqua Bun</h5>
                    <p>The Aqua Bun is found close to beaches, creeks, rivers, any body of water. Also, in places with large amounts of rain they are found to intermingle with the native land Buns. What is never seen by the average human is that Aqua Buns often enjoy swimming and diving to incredible depths in search for jewellery effect to offer trade for otehr goods and services.</p>
                    <h6>The Baywatch Bun</h6>
                    <p>The Aqua Buns lifeguard elite: the Baywatch Buns. They are solely responsible for the well-being and safety for all Aqua Buns and are the only subclass that hold such an important role in Bun culture.</p>
                    <br><br>
                    <h5>Jungle Bun</h5>
                    <p>The Jungle Bun is often found sitting atop bushes and in forested areas. The Jungle Bun has been seen by the expert Bun observers to climb trees as unimaginable speeds in order to gather seeds and resources. They do not get along well with Bird cultures and often scrap in the tree tops. Watch out for falling branches when the Jungle Bun hops from branch to branch since their legs are so much more powerful than a normal ground Bun.</p>
                    <br><br>
                    <h5>Peasant Buns</h5>
                    <p>Peasant Buns are common throughout human land and are sometimes taken into residences as "pets". In actuality, those "pets" are just recon Buns reporting information back to the Duke Bun. The Peasant Bun are the largest of Bun culture and the population is in the millions, even if we do not see them all. Bun census confirms that the population for Peasant Buns is rising. As such to rising populations comes disease due to the oversaturation of Bun hospitals. Populations fluctuate but will remain strong. The Peasant Bun are hardy and resilient; the most durable Bun.</p>
                    <br><br><br><br><br>
                    <h5>The Cinny Bun</h5>
                    <p>Round and perfect in every way.</p>

                </p>
            </div>
        </div>
    <?php require_once("copyright.php"); ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar-nav').toggleClass('active');
            $('#content').toggleClass('active');
            $(this).toggleClass('active');
        });
    });
</script>
<!-- jQuery CDN -->
<!-- Bootstrap Js CDN -->


<?php require_once("footer.php"); ?>