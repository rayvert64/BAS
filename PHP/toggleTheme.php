<?php 
   session_start();

   if(!isset($_SESSION['UserData']['Username'])){
      header("location:login.php");
      exit;
   }

   require_once("class/DBun.php");

   $DBunny= new DBun();

   $currtheme = $DBunny->getAccountTheme($_SESSION['UserData']['Username']);
   $nexttheme;

   if($currtheme == 'daytime'){
      $nexttheme = 'nighttime';
   }else{
      $nexttheme = 'daytime';

   }
   $DBunny->changeAccountTheme($_SESSION['UserData']['Username'], $nexttheme);
   $DBunny->killConn();
   header("location:account.php");
   exit;
?>