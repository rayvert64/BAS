<?php 
/* Starts the session */
require_once("header.php");

if(isset($_SESSION['UserData']['Username'])){
    header("location:account.php");
    exit;
}

require_once("class/DBun.php");

$msg;
$DBunny = new DBun();

if(isset($_POST['login'])){
    // going to need to validate the user input but for now just accept it
    $Username = htmlspecialchars(trim($_POST['uname']));
    $Password = trim($_POST['pwd']);

    if($DBunny->attempt_login($Username, $Password)){
      $_SESSION['UserData']['Username']=$Username;
      $DBunny->killConn();
      header("location:account.php");
      exit;
    }else{
      $msg = "Try Again";
      $DBunny->killConn();
    }
}elseif (isset($_POST['register'])) {
    //going to need to do server side validation on input..
    $Username = htmlspecialchars(trim($_POST['uname']));
    $FirstName = htmlspecialchars(trim($_POST['fname']));
    $LastName = htmlspecialchars(trim($_POST['lname']));
    $Email = htmlspecialchars(trim($_POST['email']));
    $Password = trim($_POST['pwd']);

    if($DBunny->accountExists($Username)) {
        if ($DBunny->createAccount($Username, $FirstName, $LastName, $Password, $Email)) {
            $_SESSION['UserData']['Username'] = $Username;
            $DBunny->killConn();
            header("location:account.php");
            exit;
        } else {
            $msg = "You attempted to register";
            $DBunny->killConn();
        }
    } else {
        $msg = "Username <strong>".$Username."</strong> is already taken";
        $DBunny->killConn();
    }
}

?>

<!-- https://bootstrapious.com/p/bootstrap-sidebar -->
<div class="wrapper">
    <?php require_once("nav.php"); ?>

    <div id="content">
        <nav id="mobile_navbar" class="navbar navbar-default">
            <div>
                <button type="button" id="sidebarCollapse" class="btn btn-outline-info"> <i class="fas fa-bars"></i></button> 
                <div id="mobile_title"><strong>Bun Alert System</strong></div>
            </div>
        </nav>
        <!-- Page content goes here.... -->
        <h2>Login and Registration Page</h2>
        
        <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModallogin">Login</button>

        <!-- Modal -->
        <div id="myModallogin" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Login</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" action="login.php">
                    <div class="form-group">
                      <label class="control-label col-sm-2" for="uname">Username:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Enter Username" name="uname" autofocus required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-sm-2" for="pwd">Password:</label>
                      <div class="col-sm-10">          
                        <input type="password" class="form-control"  placeholder="Enter password" name="pwd" required>
                      </div>
                    </div>
                    <div class="form-group">        
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default" name="login">Submit</button>
                      </div>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
        </div>

        <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalregister">Register</button>

        <!-- Modal -->
        <div id="myModalregister" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Register</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" action="login.php">

                    <div class="form-group">
                      <label class="control-label col-sm-2" for="uname">Username:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Enter username" name="uname" autofocus required>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-2" for="fname">FirstName:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Enter first name" name="fname" required>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-2" for="lname">LastName:</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder="Enter last name" name="lname" required>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-2" for="email">Email:</label>
                      <div class="col-sm-10">
                        <input type="email" class="form-control" placeholder="Enter email" name="email" required>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-sm-2" for="pwd">Password:</label>
                      <div class="col-sm-10">          
                        <input type="password" class="form-control" placeholder="Enter password" name="pwd" required>
                      </div>
                    </div>

                    <div class="form-group">        
                      <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default" name="register">Submit</button>
                      </div>
                    </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div>

        </div>
        </div>

        <!-- For Testng right now!!!! -->
        <?php 
            if(isset($msg)){
                echo "<h2> $msg </h2>";
            }
         ?>
        
    <?php require_once("copyright.php"); ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar-nav').toggleClass('active');
            $('#content').toggleClass('active');
            $(this).toggleClass('active');
        });
    });
</script>
<!-- jQuery CDN -->

<!-- Bootstrap Js CDN -->


<?php require_once("footer.php"); ?>