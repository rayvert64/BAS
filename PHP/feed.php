<?php
require_once("header.php");
?>

<!-- https://bootstrapious.com/p/bootstrap-sidebar -->
<div class="wrapper">
    <?php require_once("nav.php"); ?>

    <div id="content">
        <nav id="mobile_navbar" class="navbar navbar-default">
            <div>
                <button type="button" id="sidebarCollapse" class="btn btn-outline-info"> <i class="fas fa-bars"></i></button> 
                <div id="mobile_title"><strong>Bun Alert System</strong></div>
            </div>
        </nav>
        <!-- Page content goes here.... -->
        <h2>FEED</h2>

        <div class="row">
        <div class="col-md-3"><span style="visibility: hidden;">filler</span></div>
        <div class="col-md-6 col-sm-12">
            <div class="col-12 feed scrollbar scrollbar-primary">
                <?php
                $DBunny = new DBun();
                foreach ($DBunny->populateFeed() as $row) {
                    echo '<div class="bunInfo">';
                    echo '<div class="row">';

                    echo '<div class="col-6">X: '.$row['lat'];
                    echo '<br>Y: '.$row['lng'].'</div>';
                    echo '<div class="col-6">Time Spotted: '.$row['spottime'].'</div>';
                    echo '</div>';
                    
                    echo "<br>";

                    echo '<div class="row">';
                    echo '<div class="col-6">Colour: '.$row['colour'].'</div>';
                    echo '<div class="col-6">Gender: '.$row['gender'].'</div>';
                    echo '</div><br>';
                    echo 'Description: <br>'.$row['description'].'<br>';
                    echo '</div>';
                }
                $DBunny->killConn();
                ?>
            </div>
        </div>
        <div class="col-md-3"><span style="visibility: hidden;">filler</span></div>
        </div>
        <br>
        <div class="row">
            <div class="col-12 text-center">
                <a id="postBun" role="button" class="btn btn-primary btn-lg active" href=<?php 
                    if($status == 1) {
                        echo '"makePost.php"';
                    } else {
                        echo '"login.php"';
                    }
                ?>
                >Post</a>
            </div>
        </div>
        <br><br>
    <?php require_once("copyright.php"); ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar-nav').toggleClass('active');
            $('#content').toggleClass('active');
            $(this).toggleClass('active');
        });
    });
</script>
<!-- jQuery CDN -->
<!-- Bootstrap Js CDN -->


<?php require_once("footer.php"); ?>
