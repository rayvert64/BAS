var map, infoWindow, marker;

// Image dir
// @TODO change this when actually implementing form on page
var imgDir = "../STATIC/IMG/";
var img = "bun_pin.png";

var rabLat = document.getElementById("rabbit-lat");
var rabLng = document.getElementById("rabbit-long");


function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 49.158177, lng: -123.966739},
        zoom: 16,
        icon: imgDir+img
    });

    var pin = {
        url: imgDir+img,
        // This marker is 20 pixels wide by 32 pixels high.
        scaledSize: new google.maps.Size(32, 57),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(16, 57)
    };

    infoWindow = new google.maps.InfoWindow;

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            map.setCenter(pos);

        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }

    //create listener to create map pin
    google.maps.event.addListener(map, 'click', function(event) {
        //call function to create marker
        if (marker) {
            marker.setMap(null);
            marker = null;
        }
        console.log(event);
        marker = setMapMarker(event.latLng, pin);
    });

}

function setMapMarker(longlat, pin){
    var pin = {
        url: imgDir+img,
        // This marker is 20 pixels wide by 32 pixels high.
        scaledSize: new google.maps.Size(32, 57),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(16, 57)
    };

    var marker = new google.maps.Marker({
        position: longlat,
        animation: google.maps.Animation.DROP,
        icon: pin,
        map: map
    });

    rabLat.value = longlat.lat();
    rabLng.value = longlat.lng();

    return marker;
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}
