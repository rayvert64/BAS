var map, infoWindow, marker;

// Image dir
// @TODO change this when actually implementing form on page
var imgDir = "../STATIC/IMG/";
var img = "bun_pin.png";

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 49.158177, lng: -123.966739},
        zoom: 16,
        icon: imgDir+img
    });

    var pin = {
        url: imgDir+img,
        // This marker is 20 pixels wide by 32 pixels high.
        scaledSize: new google.maps.Size(32, 57),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(16, 57)
    };

    var infoWindows = Array();

    infoWindows[0] = new google.maps.InfoWindow;

    var i = 1;

    buns.forEach(function(b){
        b.pos = {
            lat: Number(b.lat),
            lng: Number(b.lng)
        };
        setMapMarker(b,pin, infoWindows[i]);
        i++;
    });

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            map.setCenter(pos);

        }, function() {
            handleLocationError(true, infoWindows[0], map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindows[0], map.getCenter());
    }
}

function setMapMarker(bun, pin, infoWindow){
    var pin = {
        url: imgDir+img,
        // This marker is 20 pixels wide by 32 pixels high.
        scaledSize: new google.maps.Size(32, 57),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(16, 57)
    };

    var marker = new google.maps.Marker({
        position: bun.pos,
        animation: google.maps.Animation.DROP,
        icon: pin,
        map: map,
        title: bun.bid
    });

    var bunInfo = "<div class=\"bunInfo\">" +
        "<div class=\"row\">" +
        "<div class=\"col-6\">X: " + bun.pos.lat +
        "<br>Y: " + bun.pos.lng+"</div>" +
        "<div class=\"col-6\">Time Spotted: " + bun.spottime + "</div>" +
        "</div>" +
        "<br>" +
        "<div class=\"row\">" +
        "<div class=\"col-6\">Colour: " + bun.colour + "</div>" +
        "<div class=\"col-6\">Gender: " + bun.gender + "</div>" +
        "</div><br>" +
        "Description: <br>" + bun.description + "<br>" +
        "</div>";

    infoWindow = new google.maps.InfoWindow({
        content: bunInfo
    });

    marker.addListener('click', function() {
        console.log("Test");
        infoWindow.open(map, marker);
    });

    console.log(marker);

    return marker;
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}
