<?php require_once("PHP/header.php"); ?>

<!-- https://bootstrapious.com/p/bootstrap-sidebar -->
<div class="wrapper">
    <?php require_once("PHP/nav.php"); ?>

    <div id="content">
        <nav id="mobile_navbar" class="navbar navbar-default">
            <div>
                <button type="button" id="sidebarCollapse" class="btn btn-outline-info"> <i class="fas fa-bars"></i></button> 
                <div id="mobile_title"><strong>Bun Alert System</strong></div>
            </div>
        </nav>
        <!-- Page content goes here.... -->
        <h2>BAS</h2>
        <p>Vancouver Island University is well known for its stairs and magnitude of rabbits roaming freely around
           amongst the students. They offer brief moments of pure enlightened bliss when you gaze upon their soft bunny
           selves - a moment where all the overwhelming stress of unprecedented and unreachable expectations melt away.
           It’s unfathomable how such a soft and gentle creature can play such incredible emotional support for an entire
           community of students. You can walk through campus and watch the students and faculty awe over these little
           fuzz-balls of cuteness. Nobody should be without this blissful feeling - that would be inhumane and against
           human rights. That is the focus of our project: to bring happiness to everyone in small fur coated carrot
           munching animals of joy.<p>
        <br>
        <h2>The BAS Team</h2>
        <p>We are a team of dedicated and cute-driven Bun enthusiats who are committed to the preservation and conservation of Bun Culture.<br> Through the help of the public, cataloging Bun members will help humans assist the Bun Heirarchy in ensuring a safe and welcoming future for all subsequent generations</p>
        <p>We welcome all those who share our values and goals to join and help Bun Culture! Be a part of our team and catalog some cute, fuzzy, boopersnooper Buns!</p>
    <?php require_once("PHP/copyright.php"); ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar-nav').toggleClass('active');
            $('#content').toggleClass('active');
            $(this).toggleClass('active');
        });
    });
</script>
<!-- jQuery CDN -->
<!-- Bootstrap Js CDN -->


<?php require_once("PHP/footer.php"); ?>